##                                                      Readme


## Table of contents
* [Contexte](#Contexte)
* [Outils](#outils)
* [Utilisation](#utilisation)
* [Contributeurs](#contributeurs)

## Contexte
Face à l’épidémie du COVID-19, de nombreux articles scientifiques sur lesujet ont été publiés. En moyenne plus de 2000 sont publiés par semaine depuisle mois d’avril. Afin que les médecins, chercheurs et praticiens puissent suivre etrester à jour sur cette quantité d’informations, notre objectif est donc de mettre en place une méthode de classification d’articles permettant un gain de temps afinde sélectionner les articles les plus pertinents. Ce gain de temps permet potentiellement aux lecteurs de traiter plus d’articles.
    
## Outils
Le projet a été développé avec les outils suivants:
* Python
* Keras
* Scikit-learn
* Pytorch
* Tkinter
  
## Utilisation
Pour lancer le projet, installez python :

```
$ pip install python
```

Ensuite, si ils ne sont pas présents sur votre ordinateur, installez les outils nécessaires :

```
$ pip install scikit-learn
$ pip install keras
$ pip install pytorch
$ pip install tkinter
```

Enfin, lancez la commande de l'interface :

```
$ python3 master/interfaces/interface.py
```

## Contributeurs

* Rémi : Apprentissage de représentations, LitCovid, Bert, Camembert
* Félix : modèles RNN, CNN, RNN_CNN vus en TP, représentation des textes en tokens, prétraitement des données (stopwords, lowercase, enlever les caractères spéciaux)
* Sarah : modèle RNN/CNN + Interface graphique + Adaptation du modèles SVM en classe
* Dylan : création du fichier csv pour train/test; modèle prétraitement (enlever les "stopwords" i.e. 'de', 'et', 'en'...) Embedding+LSTM sur textes tokenisés. Modèle abstrait pour les modèles adaptés pour l'interface + adapter certains modèles des notebooks vers ce nouveau format
* Eddy : modèle RNN/CNN, SVM, modèle Dense et Embedding



