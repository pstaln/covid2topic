#!/usr/bin/env python3

import pandas as pd
from sklearn.model_selection import train_test_split
import json
from random import shuffle
import numpy as np

def read_json_data(datafile, csvfile="bibliovid.csv", train=0.6, dev=0, test=0.4):
    with open(datafile) as f:
        data = json.load(f)
    topics      = []
    titles      = []
    goals_plain = []
    goals       = []
    synthesis   = []
    set_        = []
    print(len(data))
    train_dev_test = train_dev_test_split(len(data), train, dev, test)
    for i, el in enumerate(data):
        topics.append(el["topics"])
        titles.append(el["title"])
        goals_plain.append(el["goals_plain"])
        goals.append(el["goals"])
        synthesis.append(el["synthesis"])
        set_.append(train_dev_test[i])
    d = {"title": titles, "synthesis": synthesis, "goals_plain": goals_plain, "goals": goals, "topics": topics, "set": set_}
    frame = pd.DataFrame(d)
    frame.to_csv(csvfile)

def train_dev_test_split(length, train, dev, test):
    train_size = int(length * train)
    dev_size   = int(length * dev)
    print(dev, dev_size)
    test_size  = length - train_size - dev_size
    a = np.arange(length)
    shuffle(a)
    train_set = a[:train_size]
    dev_set   = a[train_size:train_size+dev_size]
    test_set  = a[-test_size:]
    index = np.empty(length, dtype=np.object)
    index[train_set] = "train"
    index[dev_set]   = "dev"
    index[test_set]  = "test"
    print(np.unique(index))
    return index

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        read_json_data(sys.argv[1])
