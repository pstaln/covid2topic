import wget
url = 'https://www.ncbi.nlm.nih.gov/research/coronavirus-api/export/tsv?filters=%7B%22topics%22%3A%5B%22General%20Info%22%5D%7D'
filename = wget.download(url, '../data/litcovid_source_general.tsv')

url = 'https://www.ncbi.nlm.nih.gov/research/coronavirus-api/export/tsv?filters=%7B%22topics%22%3A%5B%22Mechanism%22%5D%7D'
filename = wget.download(url, '../data/litcovid_source_mechanism.tsv')

url = 'https://www.ncbi.nlm.nih.gov/research/coronavirus-api/export/tsv?filters=%7B%22topics%22%3A%5B%22Transmission%22%5D%7D'
filename = wget.download(url, '../data/litcovid_source_transmission.tsv')


url = 'https://www.ncbi.nlm.nih.gov/research/coronavirus-api/export/tsv?filters=%7B%22topics%22%3A%5B%22Diagnosis%22%5D%7D'
filename = wget.download(url, '../data/litcovid_source_diagnosis.tsv')

url = 'https://www.ncbi.nlm.nih.gov/research/coronavirus-api/export/tsv?filters=%7B%22topics%22%3A%5B%22Treatment%22%5D%7D'
filename = wget.download(url, '../data/litcovid_source_treatment.tsv')



url = 'https://www.ncbi.nlm.nih.gov/research/coronavirus-api/export/tsv?filters=%7B%22topics%22%3A%5B%22Prevention%22%5D%7D'
filename = wget.download(url, '../data/litcovid_source_prevention.tsv')


url = 'https://www.ncbi.nlm.nih.gov/research/coronavirus-api/export/tsv?filters=%7B%22topics%22%3A%5B%22Epidemic%20Forecasting%22%5D%7D'
filename = wget.download(url, '../data/litcovid_source_forecasting.tsv')

url = 'https://www.ncbi.nlm.nih.gov/research/coronavirus-api/export/tsv?filters=%7B%22topics%22%3A%5B%22Case%20Report%22%5D%7D'
filename = wget.download(url, '../data/litcovid_source_case_report.tsv')

