#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 18:35:18 2020

@author: eddy
"""

import json
import numpy as np
import os
from abstract_model import AbstractModel
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import MultiLabelBinarizer
from tensorflow.keras.layers import Input, Dense, Dropout, Embedding, Conv1D, Input, LSTM, GlobalMaxPool1D, GRU, Flatten
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras import metrics
import tensorflow as tf



class DenseModel(AbstractModel):

    def __init__(self):
        self.texts = []
        self.labels = []
        self.ids = []
        self.names = []
        self.X = []
        self.Y = []
        self.X_train = []
        self.X_test = []
        self.y_train = []
        self.y_test = []
        self.size = 0
        self.model = Sequential()

    def _get_data(self):
        with open('../bibliovid.json') as json_file:
            data = json.load(json_file)
            for row in data:
                self.texts.append(row["title"]+" "+row["goals"]+" "+row["results"]+" "+row["synthesis"])
                self.labels.append([row["specialties"][j]["name"] for j in range(len(row["specialties"]))])
                for j in range(len(row["specialties"])):
                    if row["specialties"][j]["id"] not in self.ids:
                        self.ids.append(row["specialties"][j]["id"])
                        self.names.append(row["specialties"][j]["name"])


    def _preprocess_data(self):
        self.vectorizer = CountVectorizer()
        self.X = self.vectorizer.fit_transform(self.texts)
        self.X = self.X.toarray()
        self.mlb = MultiLabelBinarizer()
        self.Y = self.mlb.fit_transform(self.labels)
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X, self.Y, test_size=0.3, random_state = 42)

    def _preprocess(self, text):
        return self.vectorizer.transform([text]).toarray()


    def load_model(self):
        self._get_data()
        self._preprocess_data()

        self.model.add(Input(shape=(477,13297),dtype=tf.float32, name='input_3'))
        self.model.add(Dense(13297, activation="relu"))
        self.model.add(Dropout(0.3))
        self.model.add(Dense(len(self.Y[0]),activation='sigmoid'))

        self.model.compile(loss='binary_crossentropy',
                      optimizer='adam',
                      metrics=['acc'])
        self.model.summary()
        self._fit()

    def _fit(self):
        name = "densemodel.keras"
        if os.path.isfile(name):
            self.model = tf.keras.models.load_model(name)
        else:
            self.model.fit(self.X_train, self.y_train, epochs=3, batch_size=16, verbose=1, validation_split=0.33)
            tf.keras.models.save_model(self.model, name)


    def predict_label(self, text, threshold=0.9):
        processed_text = self._preprocess(text)
        y_pred = self.model.predict(processed_text)
        max_ = np.max(y_pred)
        threshold = threshold if max_ > 0 else 1 / threshold
        y_pred = np.array([y_pred > threshold * max_]).astype(int)[0]
        return self.mlb.inverse_transform(y_pred)[0]
