"""
Created on Wed Dec 30 18:35:18 2020

@author: sarah (+dylan adaptation abstract)
"""

import json
from joblib import dump, load
import numpy as np
import os

from abstract_model import AbstractModel
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import f1_score, classification_report

class SVM(AbstractModel):
    def __init__(self):
        self.texts = []
        self.labels = []
        self.labels_name = []
        self.ids = []
        self.names = []
        self.X = []
        self.y = []
        self.X_train = []
        self.X_test = []
        self.y_train = []
        self.y_test = []
        self.size = 0

        self.clfs = [SVC(kernel='sigmoid', C=1, decision_function_shape='ovo', probability=True),
                    SVC(kernel='poly', degree=3, C=1, decision_function_shape='ovo', probability=True),
                    SVC(kernel='rbf', gamma=1, C=1, decision_function_shape='ovo', probability=True),
                    SVC(kernel='linear', C=1, decision_function_shape='ovo', probability=True)]

    def _get_data(self):
        with open('bibliovid.json') as json_file:
            data = json.load(json_file)
            for row in data:
                self.texts.append(row["title"]+" "+row["goals"]+" "+row["results"]+" "+row["synthesis"])
                self.labels.append([row["specialties"][j]["id"] for j in range(len(row["specialties"]))])
                self.labels_name.append([row["specialties"][j]["name"] for j in range(len(row["specialties"]))])
                for j in range(len(row["specialties"])):
                    if row["specialties"][j]["id"] not in self.ids:
                        self.ids.append(row["specialties"][j]["id"])
                        self.names.append(row["specialties"][j]["name"])

    def _prepare_traindata(self):
        self.vectorizer = CountVectorizer()
        self.X = self.vectorizer.fit_transform(self.texts)
        self.X = self.X.toarray()

        self.mlb = MultiLabelBinarizer()
        yb = self.mlb.fit_transform(self.labels_name)
        yt = [int(x_, 2) for x_ in ["".join(map(str, line)) for line in yb]]
        self.trad = dict((t, b.reshape(1, -1)) for t, b in zip(yt, yb))
        self.y = yt
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X, self.y, test_size=0.3, random_state = 42)

    def _preprocess(self, text):
        return self.vectorizer.transform([text]).toarray()

    def load_model(self):
        self._get_data()
        self._prepare_traindata()
        # fit - persistence
        dump_file = "svm_model.joblib"
        if os.path.isfile(dump_file):
            self.clfs = load(dump_file)
        else:
            for clf in self.clfs:
                clf.fit(self.X_train, self.y_train)
            dump(self.clfs, dump_file)

    def predict_label(self, text):
        processed_text = self._preprocess(text)
        threshold = 0.2
        probas = []
        for clf in self.clfs:
            probas.append(clf.predict_proba(processed_text)[0])
        probas = np.mean(probas, axis=0)
        i = np.argmax(probas)
        print(self.clfs[0].classes_[i])
        print(self.trad[self.clfs[0].classes_[i]])
        print(self.mlb.classes_[self.trad[self.clfs[0].classes_[i]]])
        return self.mlb.inverse_transform(self.trad[self.clfs[0].classes_[i]])
