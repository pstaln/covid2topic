#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 18:35:18 2020

@author: eddy & dylan
"""

import json
import numpy as np
import os
from modeles.abstract_model import AbstractModel
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import MultiLabelBinarizer
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import collections
from torch.utils.data import TensorDataset, DataLoader


class RNNModel(AbstractModel):

    class RNN(nn.Module):
        def __init__(self,vocab,embed_size,hidden_size,label_vocab):
            super().__init__()
            self.embed = nn.Embedding(len(vocab), embed_size)
            self.rnn = nn.GRU(embed_size, hidden_size, num_layers=1, bidirectional=False, batch_first=True)
            self.dropout = nn.Dropout(0.3)
            self.decision = nn.Linear(hidden_size * 1 * 1, len(label_vocab))

        def forward(self, x):
            embed = self.embed(x)
            output, hidden = self.rnn(embed)
            drop = self.dropout(hidden)
            return self.decision(drop.transpose(0, 1).contiguous().view(x.size(0), -1))

    def __init__(self):
        self.texts = []
        self.labels = []
        self.ids = []
        self.names = []
        self.max_len = 8785
        self.batch_size = 64
        self.embed_size = 128
        self.hidden_size = 128
        self.device = torch.device('cpu')
        self.int_texts = []
        self.vocab = collections.defaultdict(lambda: len(self.vocab))
        self.label_vocab = []
        self.mlb = MultiLabelBinarizer()
        self.train_loader = []
        self.valid_loader = []
        self.model = None

    def _get_data(self):
        with open('../bibliovid.json') as json_file:
            data = json.load(json_file)
            for row in data:
                self.texts.append(row["title"]+" "+row["goals"]+" "+row["results"]+" "+row["synthesis"])
                self.labels.append([row["specialties"][j]["name"] for j in range(len(row["specialties"]))])
                for j in range(len(row["specialties"])):
                    if row["specialties"][j]["id"] not in self.ids:
                        self.ids.append(row["specialties"][j]["id"])
                        self.names.append(row["specialties"][j]["name"])
        self.label_vocab = {label: i for i, label in zip(self.ids,self.names)}

    def _preprocess(self, text):
        X = torch.zeros(1, self.max_len).long()
        text = [self.vocab[token] for token in text.split()]
        length = min(self.max_len, len(text))
        X[0,:length] = torch.LongTensor(text[:length])
        return X

    def _preprocess_data(self):
        self.vocab['<eos>'] = 0
        for text in self.texts:
            self.int_texts.append([self.vocab[token] for token in text.split()])
        # self.vectorizer = CountVectorizer()
        X = torch.zeros(len(self.int_texts), self.max_len).long()
        for i, text in enumerate(self.int_texts):
            length = min(self.max_len, len(text))
            X[i,:length] = torch.LongTensor(text[:length])

        Y = self.mlb.fit_transform(self.labels)
        Y = torch.LongTensor(Y)
        X = X.to(self.device)
        Y = Y.to(self.device)
        X_train, X_valid, Y_train, Y_valid = train_test_split(X, Y, test_size=0.3, random_state=42)

        train_set = TensorDataset(X_train, Y_train)
        valid_set = TensorDataset(X_valid, Y_valid)

        self.train_loader = DataLoader(train_set, batch_size=self.batch_size, shuffle=True)
        self.valid_loader = DataLoader(valid_set, batch_size=self.batch_size)

    def _perf(self, model, loader):
        criterion = nn.MultiLabelSoftMarginLoss()
        model.eval()
        total_loss = correct = num = 0
        for x, y in loader:
          with torch.no_grad():
            y_scores = model(x)
            loss = criterion(y_scores, y)
            y_pred = torch.max(y_scores, 1)[1]
            y_true = torch.max(y,1)[1]
            correct += torch.sum(y_pred.data == y_true.data)
            total_loss += loss.item()
            num += len(y)
        return total_loss / num, correct.item() / num

    def _fit(self, model, epochs):
        criterion = nn.MultiLabelSoftMarginLoss()
        optimizer = optim.Adam(model.parameters())
        for epoch in range(epochs):
            model.train()
            total_loss = num = 0
            for x, y in self.train_loader:
                optimizer.zero_grad()
                y_scores = model(x)
                loss = criterion(y_scores, y)
                loss.backward()
                optimizer.step()
                total_loss += loss.item()
                num += len(y)
            print(epoch, total_loss / num, *self._perf(model, self.valid_loader))

    def load_model(self):
        self._get_data()
        self._preprocess_data()

        self.model = self.RNN(self.vocab,self.embed_size,self.hidden_size,self.label_vocab)
        self.model.to(self.device)
        outfile = "pytorchRNN.pt"
        if os.path.isfile(outfile):
            self.model.load_state_dict(torch.load(outfile)['state_dict'])
        else:
            self._fit(self.model,3)
            torch.save({'state_dict': self.model.state_dict()}, outfile)

    def predict_label(self, text, threshold=0.9):
        #il faut transformer text en tensor
        X = self._preprocess(text)
        y_pred = np.array(self.model(X).tolist())
        max_ = np.max(y_pred)
        if max_ < 0: threshold = 1/threshold
        y_pred = np.array([y_pred > threshold * max_]).astype(int)[0]
        labels = self.mlb.inverse_transform(y_pred)[0]
        return labels
