#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 18:35:18 2020

@author: sarah
"""

import json
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import f1_score, classification_report



class SVM:

    def __init__(self):
        self.texts = []
        self.labels = []
        self.ids = []
        self.names = []
        self.X = []
        self.y = []
        self.X_train = []
        self.X_test = []
        self.y_train = []
        self.y_test = []
        self.size = 0

        self.clfs = [SVC(kernel='sigmoid', C=1, decision_function_shape='ovo'),
                    SVC(kernel='poly', degree=3, C=1, decision_function_shape='ovo'),
                    SVC(kernel='rbf', gamma=1, C=1, decision_function_shape='ovo'),
                    SVC(kernel='linear', C=1, decision_function_shape='ovo')]



    def get_data(self):
        with open('bibliovid.json') as json_file:
            data = json.load(json_file)
            for row in data:
                self.texts.append(row["title"]+" "+row["goals"]+" "+row["results"]+" "+row["synthesis"])
                self.labels.append([row["specialties"][j]["id"] for j in range(len(row["specialties"]))])
                for j in range(len(row["specialties"])):
                    if row["specialties"][j]["id"] not in self.ids:
                        self.ids.append(row["specialties"][j]["id"])
                        self.names.append(row["specialties"][j]["name"])
        #return self.texts, self.labels, self.ids, self.names

    def preprocess_data(self):
        vectorizer = CountVectorizer()
        self.X = vectorizer.fit_transform(self.texts)
        self.X = self.X.toarray()

        self.y = np.zeros((len(self.texts),max(self.ids)+1))
        for i in range(len(self.texts)) :
            for j in self.labels[i] :
                self.y[i][j]=1

        X_bin = []
        for i in range(self.y.shape[0]):
            res = ""
            for j in range(self.y.shape[1]):
                res += str(int(self.y[i][j]))
            X_bin.append(res)

        self.y = []
        for bin in X_bin:
            self.y.append(int(bin,2))
        self.y = np.array(self.y)

        my_sizes = []
        for x in self.X:
            my_sizes.append(len(x))
        self.size = np.max(my_sizes)

        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X, self.y, test_size=0.3, random_state = 42)


    def process_mydata(self, text):
        vectorizer = CountVectorizer()
        X = vectorizer.fit_transform(text)
        X = X.toarray()

        new_X = []
        if len(X[0]) < self.size:
            for i in range(self.size):
                if i < len(X[0]):
                    new_X.append(X[0][i])
                else:
                    new_X.append(0)


        new_X = np.array(new_X)
        new_X = new_X.reshape(1,new_X.shape[0])
        return new_X
       # pass

    def fit(self, mon_text):
        for clf in self.clfs:
            clf.fit(self.X_train,self.y_train)
            #y_pred = clf.predict(self.X_test)
            y_pred = clf.predict(mon_text)
            #print('score %s' % f1_score(y_pred, self.y_test, average='weighted'))
            #score = f1_score(y_pred, self.y_test,average='weighted')
        return y_pred #, score
        #return classifieur

    def predict_label(self,y_pred):
        label_vocab = {i: label for i, label in zip(self.ids,self.names)}
        binary = '{0:023b}'.format(y_pred)
        X = np.array([])
        for b in binary:
            X = np.append(X,int(b))
        result = [''.join(label_vocab[i]) for i in np.where(X==1)[0]]
        print("Etiquette :", result)
        return result


