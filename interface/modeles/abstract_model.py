from abc import ABC, abstractmethod

class AbstractModel(ABC):
    @abstractmethod
    def __init__(self, *args, **kwargs):
        """Vous pourrez mettre ici les hyperparamètres etc"""
        pass

    @abstractmethod
    def _preprocess(self, *args, **kwargs):
        """méthode qui ne servira que dans le modèle, ne pas utiliser depuis
        l'interface"""
        pass

    @abstractmethod
    def load_model(self):
        """sera appelé dans l'interface pour charger le modèle, charger les
        poids, entraîner le modèle, associer les étiquettes aux noms...
        n'hésitez pas à créer d'autres méthodes pour découper cette partie,
        propre aux besoins de votre modèle, par des noms précédés de '_' par
        exemple _fit(self, X, y) ou _load_training_data(self)..."""
        pass

    @abstractmethod
    def predict_label(self, text):
        """sera appelé dans l'interface pour prédire les étiquettes associées au
        texte non prétraité passé en entrée"""
        pass

    def __repr__(self):
        return self.__class__.__name__

    __str__ = __repr__