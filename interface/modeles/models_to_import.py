import os, sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
os.chdir(os.path.realpath(os.path.dirname(__file__)))

from model_Dense import DenseModel
from model_RNN import RNNModel
from model_Embedding import EmbeddingModel

models = [DenseModel, RNNModel, EmbeddingModel]

models = dict([(str(cls()), cls) for cls in models])
# est un dictionnaire contenant le nom du modèle en clef et la classe à
# instancier en valeur
# on instancie avec un model = models["DenseModel"]() par exemple