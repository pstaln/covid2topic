#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 23 16:33:17 2020

@author: sarah
"""

from tkinter import *
from tkinter import filedialog as fd
import os

from modeles.models_to_import import models

class Application():

    def __init__(self):
        self.window = Tk()
        self.window.title("Covid article treatment")
        self.window.geometry("1000x150")
        self.window.minsize(500,400)
        self.window.resizable(False, False)
        self.model_list = list(models.keys())

        #color of the window
        #self.window.config(background = "#BAC8E5")

             #Browse files
        def browse_files():

            filename = fd.askopenfilename(filetypes=[("txt files", "*.txt")])
            self.entry.delete(0, END)
            self.entry.insert(0, filename) #folder
            self.texte_file = open(filename)
            self.my_text = self.texte_file.read()
            self.texte_file.close()

           # Process the chosen model
        def process():
            #print("My choice ",self.all_models.get())
            chosen_model = self.all_models.get()
            print("Mon model :", chosen_model)
            chosen_model = models[chosen_model]()
            chosen_model.load_model()
            y_pred = chosen_model.predict_label(self.my_text) # liste de str
            self.result_label = Text(self.window, font=("Helvetica", 18))
            self.result_label.insert(END, "Assigned Label  : " + ", ".join(y_pred))
            self.result_label.place(x = 250, y = 150, width = 500, height = 50 )

        #frame path & place on the window
        self.path_frame = Frame(self.window,
                                borderwidth = 1, #3
                                relief = SOLID, #SUNKEN
                                #background = "#BAC8E5",
                                width = 900,
                                height = 100)
        self.path_frame.place(x= 50, y = 10)

        #Keybord input
        self.entry = Entry(self.path_frame,
                           width = 60,
                           #background = "#BAC8E5",
                           font = ("Courrier", 15))
        self.entry.place( x = 145, y = 15)
        self.entry.focus()

        # button Select txt document
        self.browse = Button(self.path_frame,
                              text="Select txt file",
                              width = 8,
                              height = 3,
                              command = browse_files)
        self.browse.place(x = 20, y = 8)

        # button process : process Ai
        self.process = Button(self.path_frame,
                              text="Process",
                              width= 8,
                              height = 3,
                              command = process
                              )
        self.process.place(x = 770, y = 8)

        # Barre de sélection de modèle
        self.all_models = StringVar(self.window)
        self.all_models.set(self.model_list[0])

        self.choice = OptionMenu(self.window,
                                 self.all_models,
                                 *self.model_list)

        self.choice.config(width='40',
                           borderwidth=2,
                           activebackground='darkorange',
                           bg='gray',
                           relief=RAISED)

        self.choice.place(x = 250, y = 60)

        #Show window
        self.window.mainloop()
        #os.system()

app = Application()
